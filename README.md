Kelompok 6:
- M. Arya Rakha
- Much. Rizki Andrianto
- Erieca Faradina Putri
- M. Luqman Hakim

Program ini merupakan program Client Server yang menjalankan protokol Compression (server) dan Decompression (client) sebelum mengirimkan dan menerima data.
Data yang akan dikirim, di kompres, lalu di decompress berupa GZip.

Pada TCP_Compress, directory default nya adalah D://ToBeCompressed
Pada TCP_Decompress, directory luaran default adalah D://Decompressed